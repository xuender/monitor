lint:
	golangci-lint run

proto:
	protoc --go_out=. pb/*.proto
