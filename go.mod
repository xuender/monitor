module gitee.com/xuender/monitor

go 1.18

require (
	github.com/xuender/oils v0.1.9
	golang.org/x/text v0.3.7
	google.golang.org/protobuf v1.28.0
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	golang.org/x/exp v0.0.0-20220518171630-0b5c67f07fdf // indirect
)
