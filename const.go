package monitor

import (
	"embed"

	"github.com/xuender/oils/base"
	"github.com/xuender/oils/i18n"
	"golang.org/x/text/language"
)

//go:embed locales
var locales embed.FS

// nolint
var Printer = i18n.GetPrinter(language.SimplifiedChinese, language.English)

// nolint
func init() {
	base.Panic(i18n.Load(locales))
}
