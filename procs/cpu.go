package procs

import "gitee.com/xuender/monitor/pb"

func Usage(cpu1, cpu2 *pb.Cpu) float64 {
	idle := cpu2.Idle - cpu1.Idle
	total := cpu2.Total() - cpu1.Total()

	var hundred uint64 = 100

	return float64((total-idle)*hundred) / float64(total)
}
