package procs_test

import (
	"fmt"
	"testing"

	"gitee.com/xuender/monitor/procs"
	"github.com/xuender/oils/assert"
	"github.com/xuender/oils/base"
)

const cpuLine = "cpu  631097 4756 271496 20211495 17658 0 3780 0 0 0"

func ExampleNewStat() {
	stat, err := procs.NewStat()

	fmt.Println(stat.Ctxt)
	fmt.Println(err)
}

func TestNewStat(t *testing.T) {
	t.Parallel()

	stat, err := procs.NewStat()

	assert.Nil(t, err)
	assert.GreaterOrEqual(t, stat.Cpu.User, 10)
	assert.GreaterOrEqual(t, stat.Cpus[0].User, 10)
	assert.GreaterOrEqual(t, stat.Ctxt, 10)
	assert.GreaterOrEqual(t, stat.Btime, 10)
	assert.GreaterOrEqual(t, stat.Processes, 10)
	assert.GreaterOrEqual(t, stat.ProcsRunning, 1)
	assert.GreaterOrEqual(t, stat.ProcsBlocked, 0)
}

func BenchmarkNewCPUByString(t *testing.B) {
	t.ResetTimer()

	for i := 0; i < t.N; i++ {
		_, _ = procs.NewCPUByString(cpuLine)
	}
}

func BenchmarkNewCPU(t *testing.B) {
	data := base.SplitSpace(cpuLine)

	t.ResetTimer()

	for i := 0; i < t.N; i++ {
		_, _ = procs.NewCPU(data)
	}
}

func TestNewCpuByString(t *testing.T) {
	t.Parallel()

	cpu, err := procs.NewCPUByString(cpuLine)

	assert.Nil(t, err)
	assert.Equal(t, 631097, cpu.User)
	assert.Equal(t, 0, cpu.GuestNice)
}

func TestNewCpu(t *testing.T) {
	t.Parallel()

	cpu, err := procs.NewCPU(base.SplitSpace(cpuLine))

	assert.Nil(t, err)
	assert.Equal(t, 631097, cpu.User)
	assert.Equal(t, 0, cpu.GuestNice)
}

func TestNewCpu_Error(t *testing.T) {
	t.Parallel()

	_, err := procs.NewCPU(base.SplitSpace("aa bb"))

	assert.NotNil(t, err)
}

func TestParseUint64s(t *testing.T) {
	t.Parallel()

	_, err := procs.ParseUint64s(base.SplitSpace("aa bb"))

	assert.NotNil(t, err)
}
