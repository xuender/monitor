package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitee.com/xuender/monitor"
	"gitee.com/xuender/monitor/procs"
	"github.com/xuender/oils/oss"
)

func main() {
	flag.Usage = usage
	flag.Parse()

	if stat, err := procs.NewStat(); err == nil {
		time.Sleep(time.Second)

		stat2, _ := procs.NewStat()

		fmt.Fprintf(flag.CommandLine.Output(), "%s\n\nCPU: %.02f%%\n", stat.Report(), procs.Usage(stat.Cpu, stat2.Cpu))
	} else {
		monitor.Printer.Fprintf(flag.CommandLine.Output(), "error_%v", err)
	}
}

func usage() {
	mod := oss.GetMod("monitor")

	monitor.Printer.Fprintf(flag.CommandLine.Output(), "monitor_%s", mod.Version)
	monitor.Printer.Fprintf(flag.CommandLine.Output(), "help")
	monitor.Printer.Fprintf(flag.CommandLine.Output(), "usage_%s", os.Args[0])
	flag.PrintDefaults()
	monitor.Printer.Fprintf(flag.CommandLine.Output(), "mod_%s sum_%s", mod.Path, mod.Sum)
}
