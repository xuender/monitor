package pb_test

import (
	"testing"

	"gitee.com/xuender/monitor/pb"
	"github.com/xuender/oils/assert"
)

func TestNewInterrupts(t *testing.T) {
	t.Parallel()

	ints, err := pb.NewInterrupts()

	assert.Nil(t, err)
	assert.GreaterOrEqual(t, len(ints), 5)
}
