package pb

import (
	"gitee.com/xuender/monitor"
)

func (p *Cpu) Report(name string) string {
	return monitor.Printer.Sprintf("cpu_body",
		name,
		p.User,
		p.Nice,
		p.System,
		p.Idle,
		p.Iowait,
		p.Irrq,
		p.Softirq,
		p.Steal,
		p.Guest,
		p.GuestNice,
	)
}

func (p *Cpu) Total() uint64 {
	return p.User + p.Nice + p.System + p.Idle + p.Iowait + p.Irrq + p.Softirq + p.Steal + p.Guest + p.GuestNice
}
