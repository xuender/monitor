package pb_test

import (
	"testing"
	"time"

	"gitee.com/xuender/monitor/procs"
	"github.com/xuender/oils/assert"
)

func TestStat_Startup(t *testing.T) {
	t.Parallel()

	stat, err := procs.NewStat()

	assert.Nil(t, err)
	assert.LessOrEqual(t, stat.Startup().Unix(), time.Now().Unix())
}

func TestStat_ToString(t *testing.T) {
	t.Parallel()

	stat, err := procs.NewStat()

	assert.Nil(t, err)
	assert.False(t, "" == stat.Report())
}
